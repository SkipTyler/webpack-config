'use strict';



document.querySelectorAll('.js-toggle').forEach( el => {
	const parent = document.querySelector('.js-parent');
	el.addEventListener('click', (e) => {
		e.preventDefault();
		parent.classList.toggle('active');
	})
});